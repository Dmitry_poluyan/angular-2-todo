import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';

import 'rxjs/add/operator/toPromise';

import {ITodo} from './todo.model';

@Injectable()
export class TodoService {
    private apiUrl = 'api/todos';

    constructor(private http:Http) {
        console.log('TodoService: constructor');
    }

    getTodos():Promise<ITodo[]> {
        console.log('TodoService: getTodos');

        return this.http
            .get(this.apiUrl)
            .toPromise()
            .then(res => res.json().data)
            .catch(TodoService.handleError);
    }

    addTodo(todo:ITodo):Promise<ITodo> {
        console.log('TodoService: addTodo');

        return this.post(todo);
    }

    saveTodo(todo:ITodo):Promise<ITodo> {
        console.log('TodoService: saveTodo');

        return this.put(todo);
    }

    deleteTodo(todo:ITodo):Promise<ITodo> {
        console.log('TodoService: deleteTodo');

        return this.delete(todo);
    }

    private post(todo:ITodo):Promise<ITodo> {
        console.log('TodoService: post');

        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers});
        let body = JSON.stringify(todo);

        return this.http
            .post(this.apiUrl, body, options)
            .toPromise()
            .then(res => res.json().data)
            .catch(TodoService.handleError);
    }

    private put(todo:ITodo):Promise<ITodo> {
        console.log('TodoService: put');

        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers});
        let body = JSON.stringify(todo);
        let url = `${this.apiUrl}/${todo.id}`;

        return this.http
            .put(url, body, options)
            .toPromise()
            .then(res => todo)
            .catch(TodoService.handleError);
    }

    private delete(todo:ITodo):Promise<ITodo> {
        console.log('TodoService: delete');

        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers});
        let url = `${this.apiUrl}/${todo.id}`;

        return this.http
            .delete(url, options)
            .toPromise()
            .then(res => todo)
            .catch(TodoService.handleError);
    }

    private static handleError(error:any):Promise<any> {
        console.log('TodoService: handleError');
        console.log('Error', error);

        return Promise.reject(error.message || error);
    }
}
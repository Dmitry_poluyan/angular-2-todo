import { Component } from '@angular/core';

import { TodosComponent } from './components/todos/todos.component';

@Component({
    selector: 'todo-app',
    templateUrl: './app/app.component.html',
    styleUrls: ['./app/app.component.css'],
    directives: [TodosComponent]
})

export class AppComponent {
    title: string;

    constructor() {
        console.log('AppComponent: constructor');
        
        this.title = 'Angular 2Do';
    }
}
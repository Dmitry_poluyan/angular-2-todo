import { Component, Input, Output, EventEmitter } from '@angular/core';

import { ITodo } from '../../../shared/todo.model';

@Component({
    selector: 'todo-item',
    templateUrl: './app/components/todos/todo-item/todo-item.component.html',
    styleUrls: ['./app/components/todos/todo-item/todo-item.component.css']
})
export class TodoItemComponent {
    @Input() todo: ITodo;
    @Output() deleted: EventEmitter<ITodo>;
    @Output() toggled: EventEmitter<ITodo>;

    constructor() {
        console.log('TodoItemComponent: constructor');
        
        this.deleted = new EventEmitter<ITodo>();
        this.toggled = new EventEmitter<ITodo>();
    }

    toggle() {
        console.log('TodoItemComponent: toggle');
        
        this.todo.done = !this.todo.done;
        this.toggled.emit(this.todo);
    }

    deleteTodo() {
        console.log('TodoItemComponent: delete');
        
        this.deleted.emit(this.todo);
    }
}
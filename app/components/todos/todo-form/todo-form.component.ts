import {Component, Output, EventEmitter} from '@angular/core';

import {ITodo, Todo} from '../../../shared/todo.model';

@Component({
    selector: 'todo-form',
    templateUrl: './app/components/todos/todo-form/todo-form.component.html',
    styleUrls: ['./app/components/todos/todo-form/todo-form.component.css']
})

export class TodoFormComponent {
    @Output() created:EventEmitter<ITodo>;

    constructor() {
        console.log('TodoFormComponent: constructor');

        this.created = new EventEmitter<ITodo>();
    }

    create(title:string) {
        console.log('TodoFormComponent: create');

        if (title) {
            this.created.emit(new Todo(title));
        }
    }
}